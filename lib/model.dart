class TestModel {
  List<StudenModel> studentdata;

  TestModel({this.studentdata});

  TestModel.fromJson( json) {
    if (json != null) {
      studentdata = new List<StudenModel>();
      json.forEach((v) {
        studentdata.add(new StudenModel.fromJson(v));
      });
    }

  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    if (this.studentdata != null) {
      data['all'] = this.studentdata.map((v) => v.toJson()).toList();
    }
    return data;
  }
}

class StudenModel {

  List<StudenModel> studentdatas;
  int id;
  String roll;
  String name;
  String discipline;
  String passingYear;
  String degree;

  StudenModel(
      {this.id,
        this.roll,
        this.name,
        this.discipline,
        this.passingYear,
        this.degree});

  StudenModel.fromJson(Map<String, dynamic> json) {
    id = json['id'];
    roll = json['roll'];
    name = json['name'];
    discipline = json['discipline'];
    passingYear = json['passing_year'];
    degree = json['degree'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['id'] = this.id;
    data['roll'] = this.roll;
    data['name'] = this.name;
    data['discipline'] = this.discipline;
    data['passing_year'] = this.passingYear;
    data['degree'] = this.degree;
    return data;
  }
}