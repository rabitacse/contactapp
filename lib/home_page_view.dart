import 'package:flutter/material.dart';
import 'package:testss/about_page_view.dart';
import 'package:testss/details.dart';
import 'package:testss/logout_page.dart';
import 'package:http/http.dart' as http;
import 'package:testss/model.dart';
import 'dart:convert';

class MyHomePage extends StatefulWidget {
  @override
  _MyHomePageState createState() => _MyHomePageState();
}

class _MyHomePageState extends State<MyHomePage> {

  TestModel studentList;
  final details=Details();

  fetchData() async {
    var url = 'http://contactdroid.000webhostapp.com/foo';
    var response = await http.get(url);
    var encodedData = jsonDecode(response.body);

    studentList = TestModel.fromJson(encodedData);

    print(studentList.studentdata.length);
    studentList.studentdata.sort((a, b) =>
        a.name.toLowerCase().trim().compareTo(b.name.toLowerCase().trim()));
    setState(() {

    });
  }

  @override
  void initState() {
    // TODO: implement initState
    super.initState();
    print('Init State Calling');
    fetchData();
  }

  @override
  void dispose() {
    // TODO: implement dispose
    super.dispose();
    print('dispose Calling');
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(),
      drawer: drawer(),
      body: studentList != null ? bodyContent() : LinearProgressIndicator(),
    );
  }

  Container bodyContent() {
    return Container(child: ListView.builder(
        itemCount: studentList.studentdata.length,
        itemBuilder: (context, index) {
          return Card(
            elevation: 4,
            child: ListTile(
              leading: CircleAvatar(
                child: Text(
                    studentList.studentdata[index].name.substring(0, 1)
                ),
              ),

              onTap: (){
                Navigator.push(context,
                    MaterialPageRoute(builder:
                    (context)=>Details(details:studentList.studentdata[index])
                    )
                );


              },
              title: Text(studentList.studentdata[index].name),
              subtitle: Text(studentList.studentdata[index].discipline),
            ),
          );
        }
    ),
    );
  }

  Drawer drawer() {
    return Drawer(
      child: ListView(
        padding: EdgeInsets.all(0),
        children: <Widget>[
          Container(
            height: 180,
            width: double.infinity,
            decoration: BoxDecoration(
                gradient: LinearGradient(
                  colors: [Colors.red.shade900, Colors.red.shade600],
                )
            ),
            child: Center(
              child: CircleAvatar(
                radius: 45,
                backgroundColor: Colors.white,
                child: FlutterLogo(size: 70,
                ),
              ),
            ),
          ),
          drawerListItem('Home', Icons.home,'home_page_view'),
          drawerListItem('About', Icons.info, AboutPage()),
          drawerListItem('Logout', Icons.exit_to_app,LogOutPage()),
        ],
      ),
    );
  }

  ListTile drawerListItem(title, icon,page) {
    return ListTile(
      title: Text(title,
        style: TextStyle(
            fontSize: 17,
            fontWeight: FontWeight.w700
        ),
      ),
      leading: Icon(icon, size: 30,),
      onTap: () {
       // Navigator.of(context).pop();
        Navigator.push(context,
            MaterialPageRoute(
                builder: (context)=>page
            )
        );
      },
    );
  }
}