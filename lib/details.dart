import 'package:flutter/material.dart';
import 'package:testss/model.dart';




class Details extends StatelessWidget {
  @override
  final StudenModel  details;

  Details({this.details});

  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text("Details Page"),
      ),
      body: Center(
        child: RaisedButton(
          onPressed: () {
            Navigator.pop(context);
          },
          child: Text(details.name),
        ),
      ),
    );
  }
}
